#!/usr/bin/env python
# This file is only used if you use `make publish` or
# explicitly specify it as your config file.
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

import os
import sys

sys.path.append(os.curdir)  # isort:skip
from pelicanconf import *  # isort:skip
from pelicanconf import YEAR  # isort:skip

SITEURL = "https://hackmeeting.org/hackit%d/" % (YEAR - 2000)

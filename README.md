Hackmeeting 2024
==================

Sources for Italian Hackmeeting 0x1B (2024) website.

Il sito viene gestito - in massima parte - dalla commissione comunicazione di hackmeeting. Se vuoi metterti in
contatto con loro, scrivi nella [mailing list](https://hackmeeting.org/hackit24/contact.html) per coordinarti
con loro. Se vuoi provare lo stesso a metterci le mani, fai pure... noi ti abbiamo avvertito!

Schema di massima
--------------------

Questo repository git viene automaticamente "sincronizzato" (continous deployment) e il risultato va a finire
su https://hackmeeting.org/hackit24/

Per fare modifiche al sito:

 - fatti un account personale su 0xacab.org
 - fai una modifica al sito
 - apri una pull request
 - quando la pull request viene approvata, e il tutto finisce in "master", la modifica è online


HowTo
-------

So you want to contribute, nice! You'll need a UNIX (Linux, BSD...) system and some proficiency with the terminal and with
[Git-based workflows](https://git-scm.com/book/en/v2).

### Setup your development environment

Clona questo repository sul tuo computer

Installa mkvirtualenv (`apt install virtualenvwrapper`, su sistemi Debian-based)

```
mkvirtualenv -p `which python3` hackmeeting-website
pip install -r requirements.txt
make all serve
firefox http://localhost:8000/
```

Also, `make help` is your friend.

For debug, `make DEBUG=1`

### Change content

Most content is in `content/pages/`. Just go there, find the relevant file, change it.

Now, `make all serve`, see the result in your browser

repeat until you like it

### Commit and push

usual git workflow

Griglia dei seminari
----------------------

I talk compaiono in questo repository come cartelle all'interno della cartella `talks/`. Tuttavia, la cosa
corretta da fare **NON** è modificare i contenuti direttamente lì, ma modificare un calendario nextcloud da cui
queste informazioni provengono.
Infatti il calendario viene gestito dalla commissione griglia: [coordinati con
loro](https://hackmeeting.org/hackit24/contact.html) se vuoi toccare questa parte del sito.

Altre modifiche al sito
-------------------------

Il sito è un'istanza di [pelican](https://docs.getpelican.com/en/latest/), più alcuni plugin custom:
 - `langmenu`, per avere un menu che punta alle versioni localizzate della pagina
 - `talks`, per gestire i talk in modo "speciale" per hackmeeting

Puoi quindi riferirti alla documentazione di pelican per fare delle prove e capire come modificare il sito.

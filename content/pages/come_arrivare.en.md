Title: How to get there
slug: come-arrivare
lang: en

Hackit {{hm.year}} will be held at [{{hm.location.name}}]({{hm.location.website}}) - [{{hm.location.address}}, {{hm.location.city}}](https://www.openstreetmap.org/#map=16/{{hm.location.geo.lat}}/{{hm.location.geo.lon}})

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox={{hm.osm.bbox}}&amp;layer=mapnik&amp;marker={{hm.location.geo.lat}}%2C{{hm.location.geo.lon}}" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat={{hm.location.geo.lat}}&amp;mlon={{hm.location.geo.lon}}#map=18/{{hm.location.geo.lat}}/{{hm.location.geo.lon}}" target="_blank">View Map</a></small>

Hackmeeting it's easy to reach. If you get lost just search for Brescia's monumental cemetery that's next to Magazzino 47.

## By train or autobus

Hackmeeting is 20 minutes walking from the train and the autobus stations.
Out of the train station is possible to take the autobus number 3 direction **Mandolossa** and the number 9 direction **Violino** that stops in via Milano, just after the cemetery.

## By car

Hackmeeting it's easy to reach from highway A4, exit Brescia Ovest.
Around the space it's possible to park for free, also for camper and vans.


## By Airplane

The closest airport is Milano Bergamo (BGY), 40km from Brescia. There are fast autobus linking the airport with Brescia train station in about 1 hour, the price for the ticket is 12€.
Another option to reach Brescia is to take an urban bus to Bergamo (3€) and then the train (5.20€) but it'll take 30 minutes more.


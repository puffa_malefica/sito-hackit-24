Title: Info
slug: info
navbar_sort: 1
lang: en


#### Where  

{{hm.location.name}}, {{hm.location.address}}, {{hm.location.city}}, Italia

[Here is how to arrive]({filename}/pages/come_arrivare.en.md)

#### When

From Friday 14 June to Sunday 16 June 2024

#### Sleeping

TBD_EN

#### Map

TBD

##### Can I arrive before Friday 14?

TBD_EN

##### Can I stay even beyond Sunday 16?

TBD_EN

#### Eating

TBD_EN

#### Who is organizing?

Hackmeeting is a yearly meeting of a community that communicates
through
[a mailing list](https://www.autistici.org/mailman/listinfo/hackmeeting). There
is no distinction between organizers and users. Everyone can subscribe
and partecipate in the organization by visiting the site
[it.hackmeeting.org](https://it.hackmeeting.org) and entering the community.

#### What is an hacker?  

Hackers are curious people, always eager to discover how things are
done. Whether it is technology or not, hackers reclaim freedom to
experiment, disassemble and reassemble things or concepts, to
understand how are they made, to improve them, and then to share how
to do it again. Hackers solve problems and build things, believing in
freedom and sharing. They do not like closed systems. Hackers' forma
mentis is not restricted to the field of software hacking: there are
people that keep the hacker mentality in every existing field, driven
by their creative impulse.

#### Who holds the talks?  

Whoever wants to. If someone wants to propose a talk, they just has to
propose it on the mailing list. If the proposal is well received, it
gets on calendar. If there are some problems, the community will be
happy to help improve the proposal.

### What’s in there, besides talks?  

There is a LAN space, as to say an area dedicated to the net: everyone
can plug their laptop, forming a network with the other participants. In
general, this is the right place to meet other attendees, to ask for
help in installing Linux, to solve a doubt, or just to have a chat.
Hackmeeting is an open-air festival, a meeting, an hacking party, a
moment of consideration, an occasion to learn something together, an act
of rebellion, an exhange of ideas, experiences, dreams, utopias.

#### How much does it cost?  

Traditionally, entrance in Hackmeeting is totally free: always keep in
mind that organizing the event has a cost. Expenses are sustained
through voluntary contributions, selling shirts and other gadgets and
sometimes through the earnings of the bar.  
Please donate whatever you can, every small donation counts.

#### What shall I bring?  

If you want to bring a computer, take a power
strip too. Do not forget networking hardware (like Ethernet cables,
switches, WiFi access points). Remember to bring the hardware that
you will want to hack in company. We will try to get some internet
connection for everybody to share, but we can't really guarantee anything.
If you think that you need it, bring a 3G/4G stick with you
and the necessary to share it with friends! Try to be as independent
as possible regarding your hardware.

#### May I take photos, make videos, post, tag, share, upload?  

We think that the freedom to choose the dimensions of one’s own private
sphere and public profile must be guaranteed to every participant: in
this spirit, photos and/or videos are admitted only if explicitly
authorized by every person that appears in the media. Nobody should be
photographed without knowing.  
Many people at Hackmeeting really care about their privacy, please ask
before taking a picture.

#### How are people expected to behave?  

Hackmeeting is a self-managed space, a temporary independent space and
whoever passes through is expected to behave according to the principles
of antisexism, antiracism and antifascism. If you are victim or witness
of an act of oppression, aggression, brute force, port scan, ping flood
and other non-consensual DOS and you do not know how to react, always
count on the community’s support and do not hesitate to attract
attention and ask for help.

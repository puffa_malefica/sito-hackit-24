Title: Call for contents
slug: call
navbar_sort: 4
lang: it

Hackmeeting è l’incontro annuale delle controculture digitali legate all’hacking e hacktivismo, rivolto a tutte le comunità e persone che vivono in maniera critica il rapporto con la tecnologia, i mass-media, i social e ogni aspetto della vita imposta.

Hackmeeting investe ogni ambito delle nostre vite e non si limita al digitale: crediamo nella diffusione del sapere e della tecnologia per conoscere, modificare e ricreare quanto ci sta attorno. Crediamo fortemente nelle pratiche di autogestione e DIY. Crediamo che la tecnologia debba rispettare le nostre volontà e non le logiche del profitto.

Hackmeeting non è un idea vaga, una generica linea guida o aspirazione. E' una reale capacità organizzativa basata sulla solidarietà, sull'inclusività e sulla condivisione delle conoscenze e degli strumenti per riprenderci tutto e riprendercelo insieme.

Hackmeeting è il contributo di ogni singola persona, non ci sono partecipantɜ ma solo organizzatorɜ.

Hackmeeting quest’anno si svolgerà a {{hm.location.city}} da venerdì 14 giugno a domenica 16 giugno presso il {{hm.location.name}}.

Se pensi di poter arricchire l’evento con un tuo contributo:
iscriviti alla mailing list di Hackmeeting <https://www.autistici.org/mailman/listinfo/hackmeeting>

Manda una mail a hackmeeting@inventati.org con:

- oggetto [Talk e titolo del talk oppure Laboratorio e titolo del laboratorio]
- durata [un multiplo di 30 minuti per un massimo di due ore]
- eventuali esigenze di giorno/ora
- breve spiegazione, eventuali link e/o riferimenti utili
- Nickname
- lingua
- necessità di proiettore e/o particolari tecnologie
- eventuale disponibilità a farti registrare [solo audio]
- altre necessità o particolari indicazioni.

Allestiremo spazi all’aperto (al coperto in caso di pioggia) muniti di amplificazione e proiettore.
Se pensi che la tua presentazione non abbia bisogno di tempi così lunghi, puoi proporre direttamente ad Hackmeeting un Ten Minute Talk dalla durata di massimo 10 minuti. Questi talk si terranno al termine delle giornate di venerdì e sabato durante la sera.

Se invece vuoi condividere le tue scoperte e curiosità in modo ancora più informale e caotico, potrai sistemarti sui tavoli collettivi dell'area LAN Space. Troverai curiosità, corrente alternata e rete via cavo. Ricordati di portare una presa multipla, un cavo di rete e quel che vorresti trovare e che ti serve.

Hai una domanda? Scrivi a [infohackit@tracciabi.li](mailto:infohackit@tracciabi.li)

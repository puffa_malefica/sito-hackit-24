Title: About
Slug: index
navbar_sort: 1
lang: it

<div class='twelve columns'>
  <div class='six columns'>
    <img src='{static}/images/locandina24_web.jpg'>
  </div>
  <div class='five columns offset-by-one'>
  <h3><br/><br/><br/>
  {{ hm.when }}<br/>
  {{ hm.location.name }} <br>{{ hm.location.city }}
  </h3><br/><br/><br/>
  <h5><a href="{filename}/pages/info.md">Informazioni ospitalità</h5></p>
  </div>
</div>

L'*hackmeeting* è l'[incontro]({filename}/pages/info.md) annuale delle controculture digitali italiane, di quelle comunità che si pongono in maniera critica rispetto ai meccanismi di sviluppo delle tecnologie all'interno della nostra società. Ma non solo, molto di più. Lo sussuriamo nel tuo orecchio e soltanto nel tuo, non devi dirlo a nessuno: l'hackit è solo per hackers, ovvero per chi vuole gestirsi la vita come preferisce e sa s/battersi per farlo. Anche se non ha mai visto un computer in vita sua.

Tre giorni di [seminari, giochi, dibattiti]({filename}/pages/programma.rst), scambi di idee e apprendimento collettivo, per analizzare assieme le tecnologie che utilizziamo quotidianamente, come cambiano e che stravolgimenti inducono sulle nostre vite reali e virtuali, quale ruolo possiamo rivestire nell'indirizzare questo cambiamento per liberarlo dal controllo di chi vuole monopolizzarne lo sviluppo, sgretolando i tessuti sociali e relegandoci in spazi virtuali sempre più stretti.

**L'evento è totalmente autogestito: non esiste chi organizza o fruisce, esiste solo chi partecipa.**

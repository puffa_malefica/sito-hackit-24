Title: Accessibilità
slug: accessibility
navbar_sort: 2

Hackmeeting è uno spazio e una comunità autogestita: tutto quello che 
troverai è autocostruito con povertà di mezzi, generalmente recuperando 
strutture abbandonate e riadeguandole il più possibile ai nostri bisogni.
Abbiamo cercato di rendere Hackmeeting più accessibile che potevamo, ma 
ci sono sicuramente ancora tante cose che non ci sono riuscite e altre 
che dovremmo migliorare. Abbi pazienza e aiutaci segnalandoci cosa non 
va, in modo da fare meglio il prossimo anno!

## Lo spazio

Hackit {{ hm.year }} si svolgerà al {{ hm.location.name }} a {{ hm.location.city }}

Il Magazzino47 è dislocato interamente al piano terra, tutte le aree sono accessibili anche tramite delle apposite rampe, il cortile è dotato di passaggio in asfalto per facilitare il passaggio per persone a mobilità ridotta.

## Aule seminari

Saranno allestiti 3 spazi seminari + un'eventuale spazio laboratorio (ma un po' piccolo)

## Dormire

Gli spazi tende e dormitorio sono senza gradini e raggiungibili tramite apposite rampe. 
Il tendone può ospitare all'incirca 80-100 persone munite di materassino, stuoia o brandina. Il tendone non può ospitare tende. Non ci sono letti a disposizione o stanze singole.
Lo spazio esterno è molto risicato e può ospitare all'incirca 20 tende o poco più. Si invita chi ne ha la possibilità a trovare sistemazioni esterne allo spazio.
Sarà disponibile uno spazio tende esterno all'evento raggiungibile a 15 min di auto.

## Bagni

Sono presenti tre turche e 1 bagno con doccia per persone a mobilità ridotta. Verranno allestite delle doccie esterne.

## Modalità aereo

C'è una piccola saletta che va sistemata (è il backstage artisti) ed è utilizzabile come sala modalità aereo.

## Clima

A giugno il clima è abbastanza variabile, si può passare da giornate di pioggia a giornate molto calde. Uno spazio seminario è al chiuso gli altri sono esterni ma comunque coperti da tendone o tettoia.

## Linguaggi

Non siamo ancora in grado di garantire un servizio di traduzioni in 
simultanea, ma Hackmeeting è frequentato da persone che parlano diverse 
lingue, compresa la LIS. Se hai bisogno, o se vuoi offrirti per 
tradurre, rivolgiti all'info point.

## Cani da assistenza

Se hai un cane che ti accompagna è il benvenuto. Troverà acqua e 
probabilmente altri cani con cui giocare. Se quest'ultima cosa dovesse 
essere un problema segnalacelo all'info point all'ingresso e cercheremo 
di limitare l'eventuale invadenza degli altri cani.

## Altre info

Nello spazio saranno presenti un po' ovunque prese per ricaricare 
qualunque dispositivo.
La musica ad alto volume sarà presente in modo continuativo solo venerdì 
sera e in un luogo lontano dagli altri spazi comuni.
Per qualsiasi altra informazione o necessità che non stiamo 
considerando, puoi rivolgerti all'info point all'ingresso.

Torna alla pagina delle [informazioni principali]({filename}info.md)

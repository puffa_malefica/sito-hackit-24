Title: Storia
slug: storia
navbar_sort: 8

####La storia dell'HackMeeting

* **1998 - Firenze - CPA Firenze Sud**

Viene organizzato il primo hackmeeting al Cpa di Firenze. [Strano Network](http://strano.net/), [Avana BBS](http://avana.forteprenestino.net/), [Ecn](http://www.ecn.org), [Freaknet](http://www.freaknet.org), Decoder, Metro Olografix e la rete
Cybernet decidono di fare un meeting perché quattro anni prima c’era stato un primo giro di vite contro gli hacker italiani. Il problema era
che i media iniziavano ad accorgersi che c’era il fenomeno digitale e identificavano il tentativo di capire la logica delle nuove tecnologie
come fenomeno “criminali”. La comunità hacker invece voleva sottolineare che il computer era uno strumento di massa, domestico e non solo
tecnico-universitario o militare, di controllo o di mercato.
L’informazione che su quella rete di pc passava, doveva essere libera. Radio Cybernet strimmava i seminari su web, prima webradio italiana. Da
allora, la radio ci sarà praticamente ogni anno. I tre giorni di Firenze sono quelli in cui e’ nato l’impianto dell’hackmeeting attuale. Il
messaggio era: usciamo dal digitale e portiamo nel reale e alla portata di tutti questioni tecniche e opportunità di comunicazione diretta;
siamo noi a dirtelo senza mediazioni (”don’t hate the media, become the media” da lì a qualche anno avrebbe affiancato “information wants to be
free”). Quell’anno per esempio esce Kryptonite, un libro che raccoglie how-to su strumenti specifici, legati alla privacy e all’anonimato
(anonimous remailer, gpg, packet radio). L’intenzione divulgativa dell’hackmeeting è chiara. All’hackmeeting partecipa tutta la scena più
rappresentativa dell’underground italiano. La prima generazione di hacker che si riconosce nel termine non nel senso più comunemente
attribuito dai media. Strumenti tecnici: corsi di linux per principianti e workshop sulla accessibilità delle tecnologie e dei siti.

* **1999 - Milano - Deposito Bulk**

Viene deciso collettivamente di uscire dalla dimensione dei tre giorni per darsi spazi tutto l’anno, e dare origine agli hacklab dove
incontrarsi, avere un laboratorio, condividere il lavoro, divulgare tematiche. Ospite di rilievo è Wau Holland, un hacker libertario
cofondatore del Chaos Computer Club, che senza carta di identità arrivò in Italia in modi rocamboleschi. Connessioni internazionali vengono
fatte conoscere alla comunità italiana. Strumenti tecnici: si ricorda il seminario su attaccare i protocolli di comunicazione (ip icmp tcp udp +
servizi), per capire/spiegare un po’ meglio come funzionano i protocolli di rete e quindi dove e perché sono vulnerabili. Un altro tema che
diventerà ricorrente negli hackmeting.

* **2000 - Roma - Forte Prenestino**

Sede di Avana BBS, primo hackmeeting organizzato con gli hacklab, diventati ormai realtà concrete. Jaromil si presenta con asciicam,
costruendo le relazioni declinate in altri ambiti come la net art, aspetto artistico, giocoso e capace di critica dell’hacking. È anche un
hackmeeting dedicato ai diritti in rete, alla cooperazione sociale, all’accessibilità in rete. Molti i seminari su etica e web.

* **2001 - Catania - Freaknet media lab**

Un posto particolare, visto che era nato prima degli hacklab e già si apriva al pubblico con i bollettini meteo per i pescatori, via internet,
e le tastiere in arabo per gli immigrati. È l’hackmeeting più forte dal punto di vista del numero di ore passato sul codice. Viene presentato il
progetto autistici.org/inventati.org come server autogestito che si affiancava a ecn. Viene presentato anche Bolic1, che diventerà poi
Dynebolic (the media hacktivist tool). Il Freaknet propone nell’università scientifica di Catania che si adotti il software libero
come software di base e all’hackmeeting viene proposto di estenderlo anche ad altre città. Passeranno per questo meeting anche quelli che poi
creeranno il media center del G8. Vengono proposti strumenti e persone che poi si coaguleranno per dare vita a media come Indymedia. Si inizia
a parlare di reddito e lavoro nella net economy. Strumenti tecnici: si tiene un seminario di reverse engineering, ovvero
modificare il comportamento di un programma senza conoscere il codice sorgente.

* **2002 - Bologna - TPO**

Hackmeeting mondano. la parola mediattivismo è uscita e diffusa, Indymedia è un media consolidato, NGvision rappresenta un archivio video
su web consistente, ci sono le telestreet, l’hackmeeeting diventa non più solo da hacker ma da attivismo media, e non a caso compaiono le
donne. Viene Richard Stallman. Strumenti tecnici: viene presentato il Retrocomputing ovvero la passione per il recupero dell’hardware.

* **2003 - Torino - Barrio**

Viene fatto il mitico seminario di stiraggio acrobatico e la questione genere/tecnologie viene proposta dal Sexy shock di Bologna, collettivo
di donne, sottolineando che ci sono questioni di genere anche tra hacker e che sarebbe il caso di occuparsene. Alle aule vengono dati nomi
storici dell’anarchismo. Strumenti tecnici: Honeypots, sicurezza informatica sul web sono tra gli argomenti trattati. Wireless diffuso.

* **2004 - Genova - Buridda**

Si apre ulteriormente l’ambito, vengono presentati seminari di robotica, ma anche sulla prostituzione. E sopratutto a partire da Reload di Milano
viene lanciato il concetto di reality hacking, hacking della vita quotidiana e della politica, chiedendo agli hacklab di uscire dagli
stretti ambiti nerd per entrare in quelli più allargati dell’agire politico. Strumenti tecnici: Bluetooth security uno dei seminari.

* **2005 - Napoli - TerraTerra**

Viene presentato Open non è free, il secondo libro che esce dalla comunità di hackmeeting. Visto che nella società iniziano a circolare i
concetti di open source e di free software, la comunità sente la necessità di sottolineare la necessità di approcciare in maniera critica
queste tematiche che sono comunque ormai assorbite anche dal mercato. Strumenti tecnici: introduzione al Phreaking, ovvero come telefonare
gratis, dal fischietto di Captain Crunch al vOIP.

* **2006 - Parma - Collettivo Mario Lupo**

Gli hacklab non sono più l’elemento principale di organizzazione ma c’è una percezione più comunitaria che permette l’occupazione di uno spazio
per la durata dell’hackmeeting. L’occupazione avviene in un momento ambiguo e cosparso di sgomberi nel territorio parmense e prende la forma
politica di una TAZ, anche se c’era la speranza di fare molto di più (tenere il posto, che poi invece verrà sgomberato). Nell’ambito dei
seminari si apre ulteriormente l’applicazione della filosofia hack in ambiti vari, per esempio, serpicanaro e la licenza open per la
produzione materiale, bucare un sistema che non è solo informatico ma reale e di mercato. Viene presentato The darkside of Google, altro libro
scritto da persone della comunità su aspetti ancora (allora) poco noti di Google. Strumenti tecnici: Web Semantico e Ontologie informatiche; si discute di Copyleft e fightsharing.

* **2007 - Pisa - Rebeldia**

Rispetto all’anno precedente si recupera di nuovo sul profilo internazionale, vengono Emmanuel Goldstein, parte della scena americana storica dei phreakers e media hacktivist. Andy Muller Maghun, membro del CCC, esordisce dicendo “il CCC nasce come progetto politico”, segno di come in Europa sin dall’82 si considerava il potenziale politico dell’hacking come concetto, Armin Medosh, che fa una ricostruzione storica in chiave marxista dell’evoluzione tecnologica. Appaiono seminari che impongono una presa di coscienza collettiva ecologica: viene fatto il seminario “hack the bread”, che riporta l’accento su pratiche politiche anche a livello personale. Viene introdotto lo spazio capanne dei suchi, uno spazio diverso dal workshop, di discussione libera e lavoro in comune. Strumenti tecnici: metodi di compromissione dell’anonimato, come difendersi. Voip Security, necessità di attenzione anche su Voip. IPSec, Meccanismi per proteggersi da attacchi basati sull’analisi statistica del traffico (Web site fingerprinting, etc ).

* **2008 - Palermo - AsK 191**

Hackmeeting importante per il luogo, l’incontro con le persone dell’Ask ha prodotto un momento di autogestione in comune molto forte. Conferma
spirito forte comunitario, non molto aperto perché non c’era risposta da parte del pubblico palermitano. L’impronta ecologica continua, si parla
di compost, di rifiuti, di energia solare e onde radio. Sharing not exploiting: Prosumer vs. Corporation, riflessione su social network e
web 2.0, ritorni economici delle corporation. Dal punto di vista dell’immaginario compare il workshop sullo steampunk, il dopo cyberpunk.
Strumenti tecnici: Web semantico, come approccio al web 3.0

* **2009 - Rho - Fornace**

Di nuovo l’hackmeeting va a supportare un’autogestione in difficoltà. Lo scenario è Rho, hinterland milanese devastato dalla speculazione Expo,
un enorme capannone vuoto occupato da pochi giorni. Si parla di paura, di creazione della paura, di come uscire da questo modello sociale di
persone spaventate, che si guardano in cagnesco. L’icona dell’hackit è il babau, il cattivo delle favole. Si stabilizza all’interno delle
tematiche l’interesse per la questione precariato e modelli di vita conseguenti. Si inaugura la pratica dei warm up, seminari che introducono nelle settimane precedenti sia nella città ospite, che nel resto d’Italia l’evento principale. A livello di immaginario si parla di fantascienza, di retrocomputing e restauro, videogiochi per cellulari. Strumenti tecnici: streaming audio/video giss, sniffjoke, reti gsm, reti mesh

* **2010 - Roma - La Torre**

Hackmeeting tra gli ulivi nel caldo romano, tutti in tenda, ci si riprende dall’hinterland milanese dell’anno prima. Si parla di controllo, di quanto ne abbiamo intorno, di quali strumenti utilizza. Le tematiche ecologiste rientrano dalla porta principale con il seminario sulle pale eoliche, la presentazione della rete per l’autocostruzione e il seminario sugli orti urbani. Nel lan space presenti sempre più arduini, e un modello di elicottero radiocomandato. Ospite dell’hackit Margaret killjoy, un buffo personaggio dello Steampunk Magazine, un rivista autoprodotta americana, presentata assieme a Ruggine, una rivista autoprodotta italiana di racconti e disegni. Affollatissimo il seminario di pr0n, misterisca e intrigante estensione di firefox. Strumenti tecnici: ipv6 e reti mesh, opencv, arduino.

* **2011 - Firenze - csa nExt Emerson**

Il tema di questa edizione è stato quello dell’apocalisse. Dalle nuvole dei disastri nucleari alle nuvole del cloud computing: la tecnologia e la conoscenza quando centralizzate per interessi economici e politici e in contrasto con le aspirazioni individuali e collettive di autonomia portano inevitabilmente alla… apocalisse.

* **2012 - L’Aquila - Asilo Occupato**

Una presenza significativa in una città come L’Aquila che non assomiglia più ad una città. Hack the town!! Se non è più possibile riparare i danni, dare un senso a ciò che è andato distrutto, hackmeeting prova a capire se è possibile farla funzionare in un altro modo, partendo dalla ricostruzione dei tessuti sociali e relazionali, delle connessioni vitali della città.

* **2013 - Cosenza - Ex-Officine**

Si è tenuto nell’area occupata delle ex officine Ferrovie della Calabria ed ha avuto come tema la “iattura del controllo”. L’idea dell’Hackmeeting 2013 era quello di stimolare una nuova saggezza popolare 2.0 per far fronte alle forze avverse che minacciano le libertà di espressione e di condivisione nella rete. Neanche a farlo apposta, fra iatture e superstizioni, proprio nei giorni di quest’hackmeeting escono le prime rivelazioni di Snowden.
L’attività di chi controlla i movimenti in rete, per affari, o per controllo è il tema chiave, ma gli argomenti trattati sono stati molteplici: dalla privacy alle tecnologie di comunicazione, dalla contrasessualità alle relazioni di inchiesta sui software spia utilizzati da governi e non. Degno di nota è anche l’esperimento di media trolling che porta hackmeeting sulle pagine dei più importanti quotidiani nazionali attraverso un divertente e totalmente infondato “scoop”.

* **2014 - Bologna - XM24**

Xm24 ospita gli ultimi 3 giorni di un hackmeeting durato molto più a lungo:
l'articolato percorso di warmup cittadino, coordinato dall'hacklabbo ma che
vede la partecipazione di molte realtà, antagoniste e non, porta ad un'edizione
particolarmente partecipata.

Grazie alla lunga storia mediattivista di Xm24, la visibilità e il carattere
universitario di Bologna e alla rete di comunità che si intrecciano nella lista
hackmeeting prende vita un evento che dimostra alla sua stessa comunità come la
scena sia tutt'altro che morta, ma anzi particolarmente attenta e attiva.
Durante le tre giornate la necessità e la centralita' di un evento costruito
dal basso viene riaffermato con forza non soltato dalla partecipazione della
comunità storica, ma anche dalle assemblee di comunita' trasversali ad
hackmeeting(eg. quella delle WCN italiane), l'affacciarsi di molti volti nuovi
e i moltissimi talk di grande attualità (eg. Citizen Lab con il lavoro sulla
sorveglianza digitale).

PS: il logo è un non morto, non un gorilla, non un fattone ;)

* **2015 - Napoli - Mensa occupata**

Dopo dieci anni Hackmeeting torna a Napoli e si riprende il centro della città.

La collocazione centralissima della Mensa Occupata favorisce lo svolgimento di un hackmeeting di livello non esclusivamentetecnico e molto divulgativo. A meno di due mesi del primo maggio e delle lotte contro Expo, la riflessione sul lato più politico e militante dell'hacking si impone: tecniche di autodifesa neuro-digitali, studio delle legislazioni in materia di intercettazioni, Tor, crittografia e resistenza digitale. Alla comunità si è aggiunta tanta gente nuova e questa diventa l'occasione per reinventarsi.  Si scopre che i fritti favoriscono la concentrazione.

* **2016 - Pisa - Polo Fibonacci**

Polo Fibonacci, l'hackmeeting si ritrova a Pisa il 3-5 Giugno e occupa un polo
universitario riempiendolo di giochi, feste, workshop e dibattiti. Per parlare di
tecnologia, di privacy, di sicurezza, ma anche di differenze, di gender e di
piu' inclusione all'interno del hackmeeting.

Si analizzano malware, si discute di crittografia quantistica e
ci si ritrova a giocare a retrogame alle 3 di mattina tra musica punk e la
birra che ormai e' finita da un po'

I bagni non divisi per genere si chiamano come gli editor: VIM, NANO o EMACS, mentre
alcune faccie internazionali e non rispondo ad email e scrivono pezzi di python nel mezzo del Lan Space
mentre altri cercano per la prima volta di installare Linux.

L'attenzione si sposta parecchio sui malware, anche detti captatori informatici
si prova ad analizzarli e vedere la demo di hacking-team, poi Ippolita presenta [Anime Elettriche](http://www.vita.it/it/interview/2016/04/27/anime-elettriche-corpi-digitali-linee-di-fuga-e-tattiche-di-resistenza/52/) e la sera 
alcuni propongono giochi sulla comprensione del consenso, tavole rotonde sulle
discussioni di genere e tentativi di migliorare la comunita'
Poi all'improvviso e' gia' domenica e abbracci e baci, si accolla il prossimo
hackit ai Torinesi, ci vediamo in Val Susa.

* **2017 - Venaus, Val di Susa - Borgata 8 Dicembre e Presidio permanente**

E' il ventesimo hackmeeting e per celebrare l'occasione, si pensa di
farlo in un posto diverso. L'idea gira in lista, piace subito e così si parte.
Si impara a saldare, a fare il dado vegetale, a costruire un'antenna, a
farsi il formaggio con il latte di capra.
Si parla di sicurezza digitale, di anonimato, di fisica quantistica, di
cyberspionaggio, di pokemon, di controllo, di radio e di reti mesh.
Ci si mischia con i resistenti valsusini, ci si contagia, si scambiano
racconti, esperienze, sogni.La comunità condivide con la valle, la valle si racconta alla comunità.
Si dorme in tenda si cucina e ci si lava all'aperto, si cena al cantiere
di Chiomonte, si cammina in montagna.

Tagliare le reti, insomma, ci sta! 

* **2018 - Buridda Genova**

Dopo 14 anni hackmeeting torna a Genova, nelle stanze del Buridda.
L'impronta del luogo è sicuramente l'attenzione all'autocostruzione, ma
non mancano dibattiti storici come la presentazione del numero di
Zapruder (numero dedicato all'hacking), una spiegazione pratica degli
avanzamenti crittografici negli ultimi anni, un workshop con pc e USB
alla mano come quello su Tails ed anche confronti interessanti e
contraddittori sui temi tecnici e politici come quello sul voto elettronico.
Viene presentato Stop al Panico, una raccolta di suggermenti tecnici e
legali da affrontare in caso di sequestro, si parla di sistemi operativi
alternativi per cellulari.
Si discute di social, di nolike e dell'uso tossico di questi strumenti,
riflettendo su come riappropriarci delle tecnologie "social" anche
presentando l'esperimento Bolognese di mastodon.bida.im
Dopo 3 giorni intensi, l'assemblea della domenica è molto partecipata:
scopriamo che i posti che hanno ospitato l'hackmeeting riprendono
energie mentre in varie città stanno nascendo vari hacklab mossi non
solo dalla volonta di smontare e comprendere la tecnologia come un tempo
o dalla mancanza di internet in casa, ma dal desiderio di incontrarsi,
discutere e condividere le impressioni su questi strumenti che oggi
abbiamo intorno e cercare alternative comuni insieme.
Ultimo giro al bar, dove quest'anno la birra non e' ancora finita, poi
una tuffo al mare per chi riesce, saluti sparsi a tutte le creature di
questo hackmeeting con la promessa di rivederci presto a sud a nord
online o offline, non fa differenza, l'importante e' che ci siamo.

* **2019 - Firenze - csa nExt Emerson**

* **2020 - Roma - C.S.O.A Forte Prenestino**

* **2021 - Bologna - Casona di Ponticelli**

* **2022 - Torino - C.S.O.A Gabrio**

* **2023 - Gallico Marina, Reggio Calabria - CSOA Angelina Cartella**

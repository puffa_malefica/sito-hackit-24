Warmup
======

:slug: warmup
:navbar_sort: 5
:lang: it

Cosa sono
"""""""""""""""""""""""""""""""""""""""

I warmup sono eventi "preparatori" ad hackmeeting. Avvengono in giro per l'Italia, e possono trattare gli argomenti più disparati.

Proporre un warmup
"""""""""""""""""""""""""""""""""""""""

Vuoi fare un warmup? ottimo!

* iscriviti alla `mailing list di hackmeeting <https://www.autistici.org/mailman/listinfo/hackmeeting>`_.
* scrivi in mailing list riguardo al tuo warmup: non c'è bisogno di alcuna "approvazione ufficiale", ma segnalarlo in lista è comunque un passaggio utile per favorire dibattito e comunicazione.


Elenco
"""""""""""""""""""""""""""""""""""""""
.. contents:: :local:

==========================

Oooh issaa! ...ooooh issaaa! il MIAI salpa!
--------------------------------------------

Naviganti! dal primo marzo scioglieremo le vele e tireremo su l’ancora. Si riparte con l’allestimento del MIAI! Fino ad ora abbiamo pulito, ristrutturato, ammobiliato, allestito parte della Biblioteca e realizzato un affresco :) Si è fatta ora di affrontare la massa. La quantità di materia informatica che attende ormai da troppo tempo di essere liberata dal suo involucro di plastica e cartone per ritornare alla luce nel pieno del suo splendore. I pallet sono lì...intombati...aspettano soltanto noi!

Grazie anche ad Hackmeeting abbiamo resistito.
Questo museo è frutto anche di questa comunità.

per chiarezza, i primi fine settimana dei prossimi mesi sono piccoli Warm Up di costruzione del MIAI in attesa dell'appuntamento Hackmeeting a Brescia :)

E' l'occasione per scoprire i reperti, familiarizzare, prendersene cura, riaccenderli.

* Ecco le date dei venerdì-sabato-domenica:

1-2-3 marzo

5-6-7 aprile

3-4-5 maggio

31 maggio 1-2 giugno

5-6-7 luglio inizia il Cool Down con sessione di restauro del GE-120 da tenersi il 26-27-28 ultimo fine settimana di luglio.

Questi sono appuntamenti fissi, ma se qualcun in transito volesse fermarsi in date diverse, male non fa!

Per informazioni su ospitalità e altro scrivete a info@verdebinario.org

Ti invitiamo a salire a bordo e a dare una mano in questa impresa! 

Obiettivo?

Allestire il Museo Interattivo di Archeologia Informatica - MIAI.

Quando?

Ogni primo fine settimana del mese da marzo a luglio, qui al MIAI, in Calabria, a Rende (CS) -Via C.B.Cavour 4.

Ospitalità?

Provate a organizzarvi e nel caso vogliate un supporto contattateci in privato.

E se non posso venire?

Siamo liet* anche di un piccolo/grande contributo monetario che è utile per affrontare gli imprevisti di un allestimento di questo genere

E alla fine?

Cool Down! Dopo Hackmeeting 2024, una sessione di restauro del GE-120 da tenersi il 26-27-28 ultimo fine settimana di luglio!

E alla fine fine?

Arrivare in porto, attraccare e vivere il MIAI funzionante!


Museo Interattivo di Archeologia Informatica - MIAI https://miai.musif.eu/


Assemblea di istanza mastodon bida.im e pranzo di autofinanziamento
-------------------------------------------------------------------

Domenica 19 maggio 2020 dalle 12.00 @ Spazio Libero Autogestito @Vag61 (in via Paolo Fabbri 110 a Bologna)

https://bida.im/news/quarta-assemblea-generale-distanza-mastodon-bida-im/



Presentazione del libro Cyber Bluff
------------------------------------

Sabato 18 maggio @ Laboratorio Urbano Popolare Occupato (in Piazza Pietro Lupo 25, Catania)

Presentazione del libro Cyber Bluff: storie rischi e vantaggi della rete per navigare consapevolmente (2021, eris). Discuteremo e approfondiremo insieme all'autore Ginox dei temi trattati nel libro.

https://www.attoppa.it/event/presentazione-di-cyber-bluff



Presentazione Antennine/Ninux in appennino + cena benefit
----------------------------------------------------------

Lunedì 13 Maggio @ Circolo Anarchico Berneri, Bologna (BO)

https://circoloberneri.indivia.net/events/event/presentazione-del-progetto-antennine-in-appennino-bolognese-e-cena-di-autofinanziamento



Brugole e merletti
-------------------

10-11-12 maggio @ NextEmerson Via di Bellagio 15 - Firenze

https://doityourtrash.noblogs.org/



Instant Messaging e sicurezza
------------------------------

10 maggio a partire dalle 21:00 @ Pianoterra Lab, Milano
https://www.pianoterralab.org/events/a-k-m-e-instant-messaging/

Sono cambiati i mezzi con i quali comunichiamo: sempre meno si usano email, sempre di più le app da cellulare, in particolare, per la messaggistica istantanea. L’Instant Messaging è diventato il sistema di comunicazione più diffuso, anche nell’organizzazione del lavoro. Usati spesso da attivist* e agitator* a causa della loro enorme diffusione, questi sistemi immediatamente disponibili sono diventati un obbiettivo appetibile. La vulnerabilità di queste applicazioni può così mettere a rischio la libertà, la privacy e la sicurezza delle persone. Crittografia asimmetrica, metadati, immagini, backup, codice sorgente, ne discutiamo in una tavola rotonda allargata sulla base di strategie e alternative disponibili.

con samba – underscore hacklab (Torino)

10 maggio a partire dalle 21:00

A-K-M-E

https://akme.vado.li



Workshop mappe dalla carta al digitale
---------------------------------------

Martedì 9 aprile @ Vag 61, via Paolo Fabbri 110, Bologna (BO)

https://ciemmona.noblogs.org/workshop-mappe-dalla-carta-al-digitale/

Stampa
#########

:slug: press
:navbar_sort: 9
:lang: it

Cartella stampa
""""""""""""""""""""""""""""""""""""""


Propaganda
"""""""""""""""""""""""""""""""""""""""

I materiali utili per la diffusione di hackmeeting {{hm.year}} sono nell'apposita pagina `Propaganda
<{filename}propaganda.md>`_


Foto
"""""""""""""""""""""""""""""""""""""""


.. image:: images/press/2016-IMG_0578.jpg
    :height: 200px
    :alt: La bacheca dei seminari dell'hackmeeting 2016, a Pisa
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0578.jpg

.. image:: images/press/2016-IMG_0581.jpg
    :height: 200px
    :alt: Sessione di elettronica digitale
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0581.jpg

.. image:: images/press/2016-IMG_0584.jpg
    :height: 200px
    :alt: Programmazione
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0584.jpg

.. image:: images/press/2016-IMG_0586.jpg
    :height: 200px
    :alt: Computer
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0586.jpg

.. image:: images/press/2016-IMG_0589.jpg
    :height: 200px
    :alt: Il LAN party: un posto dove sperimentare insieme
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0589.jpg

.. image:: images/press/2016-IMG_0574.jpg
    :height: 200px
    :alt: Un hack su Emilio: il famoso robottino degli anni '90 è stato riprogrammato
    :target: https://hackmeeting.org/hackit16/images/photos/IMG_0574.jpg


Comunicati stampa
"""""""""""""""""""""""""""""""""""""""




Comunicato Stampa
=================================

HACKMEETING
{{hm.when}}

TBD

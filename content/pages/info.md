Title: Info
slug: info
navbar_sort: 1

#### Dove

{{hm.location.name}}, {{hm.location.address}}, {{hm.location.city}}, Italia

[Qui trovi tutte le informazioni su come arrivare]({filename}/pages/come_arrivare.md)

#### Quando

Da Venerdì 14 Giugno a Domenica 16 Giugno 2024

#### Dormire

All'interno dello spazio l'area tendone è stata adibita a spazio dormitorio (al coperto), all'interno del tendone sarà possibile pernottare con brandine, stuoie e materassini (niente tende). La disponibilità del tendone è di circa 100 posti letto.
Nelle poche aree verdi del centro sociale sarà possibile campeggiare per un max di 20 tende piccole.
Vista la scarsa capienza del magazzino47 per il pernottamento chiediamo a chi ne ha le possibilità di trovare una sistemazione alternativa nelle vicinanze (ostello, b&b, appartementi, amici e parenti).

Per chi comunque volesse dormire nello spazio si consiglia di munirsi di:
- tappetino,stuoia, materassino (suolo in cemento)
- tappi per le orecchie (lo spazio è piccolo ed è facile essere disturbati)
- antizanzare
- brandine e maschere per dormire (per i più attrezzati)


Ci stiamo organizzando per avere anche uno spazio esterno all'evento a circa 15 minuti di auto dove sarà possibile campeggiare liberamente in area verde con servizi igenici annessi.

#### Mappa

[Clicca qui per aprire la mappa]({static}/images/map.png)

#### Posso arrivare prima di Venerdì 14?

E' possibile arrivare ad hackmeeting anche nei giorni precedenti all'evento per dare una mano. Per chi ha intenzione di arrivare durante la settimana può segnarsi sul seguente pad: <https://pad.cisti.org/p/hackit0x1b-arrivi>

#### Posso rimanere anche oltre Domenica 16?

E' possibile rimanere fino a lunedì all'interno degli spazi del Magazzino47.

#### Mangiare

Saranno garantite anche colazioni/pranzi/cene a un prezzo popolare.
La cucina sarà vegana, con alternative celiache se avete delle necessità particolari (allergie o altro) segnalatecelo.

#### Lavarsi

C'è una doccia nel bagno per mobilità ridotta e verranno allestite delle doccie esterne. Lavatevi poco che fa male.

#### Hackmeeting è accessibile?

Speriamo di sì! Trovi tutte le informazioni nella [pagina dedicata
all'accessibilità]({filename}accessibilita.md)

#### Chi organizza l'hackmeeting?

L’hackmeeting è un momento annuale di incontro di una comunità che si riunisce
intorno a [una mailing list](https://www.autistici.org/mailman/listinfo/hackmeeting).
Non esistono chi organizza e chi fruisce. Tutte le persone possono iscriversi e partecipare
all’organizzazione dell'evento, semplicemente visitando il sito [hackmeeting.org](https://hackmeeting.org) ed entrando nella comunità.

#### Chi è un@ hacker?

Le persone cosiddette hacker sono persone curiose, che non accettano di non poter mettere le mani
sulle cose. Che si tratti di tecnologia o meno chi è hackers reclama la libertà
di sperimentare. Smontare tutto, e per poi rifarlo o semplicemente capire come
funziona. Sono persone che risolvono problemi e costruiscono le cose, credono nella
libertà e nella condivisione. Non amano i sistemi chiusi. La forma mentis
di chi è hacker non è ristretta all’ambito del software-hacking: ci sono persone
che mantengono un attitudine all'hacking in ogni campo dell’esistente, sono persone curiose spinte
da un istinto creativo.

#### Chi tiene i seminari?

Chi ne ha voglia. Se qualche persona vuole proporre un seminario, non deve fare altro
che proporlo in lista. Se la proposta piace, si calendarizza. Se non piace, si
danno utili consigli per farla piacere.

#### Ma cosa si fa, a parte seguire i seminari?

Esiste un “LAN-space”, vale a dire un’area dedicata alla rete: ogni persona arriva
col proprio portatile e si può mettere in rete con altre persone. In genere in
questa zona è facile conoscere altre creature curiose, magari disponibili per farsi aiutare a
installare Linux, per risolvere un dubbio, o anche solo per scambiare quattro
chiacchiere. L’hackmeeting è un open-air festival, un meeting, un hacking
party, un momento di riflessione, un’occasione di apprendimento collettivo, un
atto di ribellione, uno scambio di idee, esperienze, sogni, utopie.

#### Quanto costa l’ingresso?

Come ogni anno, l’ingresso all’Hackmeeting è del tutto libero; ricordati però
che organizzare l’Hackmeeting ha un costo. Le spese sono sostenute grazie ai
contributi volontari, alla vendita di magliette e altri gadget e in alcuni casi
all’introito del bar e della cucina.

#### Cosa devo portare?

Se hai intenzione di utilizzare un computer, portalo accompagnato da una
ciabatta elettrica. Non dimenticare una periferica di rete di qualche tipo
(cavi ethernet, switch, access point). Ricordati inoltre di portare tutto
l’hardware su cui vorrai smanettare con gli altri. Durante l’evento la
connessione internet sarà limitata quindi, se vuoi avere la certezza
organizzati portando il necessario per te e per chi ne avra' bisogno.
In generale ogni persona organizza e partecipa cercando di essere autosufficiente
sul lato tecnologico portandosi l'hardware o costruendoselo al hackmeeting!

#### Posso scattare foto, girare video, postare, taggare, condividere, uploadare?

Pensiamo che ad ogni partecipante debba essere garantita la libertà di
scegliere in autonomia l’ampiezza della propria sfera privata e dei propri
profili pubblici; per questo all’interno di hackmeeting sono ammessi fotografie
o video SOLO SE CHIARAMENTE SEGNALATI E PRECEDENTEMENTE AUTORIZZATI da tutte e
tutti quanti vi compaiano.

Le persone che attraversano hackmeeting hanno particolarmente a cuore il concetto di privacy: prima di fare
una foto, esplicitalo!

#### Come ci si aspetta che si comportino tutte e tutti?

Hackmeeting è uno spazio autogestito, una zona temporaneamente autonoma e chi
ci transita è responsabile che le giornate di hackit si svolgano nel rispetto
dell’antisessismo, antirazzismo e antifascimo. Se subisci o assisti a episodi
di oppressione, aggressione, brute force, port scan, ping flood e altri DoS non
consensuali e non sai come reagire o mitigare l’attacco, conta sul sostegno di
tutta la comunità e non esitare a richiamare pubblicamente l’attenzione e
chiedere aiuto.

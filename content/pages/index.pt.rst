About
###################

:slug: index
:navbar_sort: 1
:lang: pt

.. raw:: html

  <div class='twelve columns'>
     <div class='six columns'>
       <img src='{static}/images/locandina24_web.jpg'>
    </div>
    <div class='five columns offset-by-one'>
    <h3><br/><br/><br/>
    {{ hm.when }}<br/>
    {{ hm.location.name }}, {{ hm.location.city }}
    </h3><br/><br/><br/>
    <!-- <h5><a href="{filename}/pages/info.md">Informazioni ospitalità</h5></p> -->
    </div>
  </div>


Hackmeeting é o encontro anual das contro-culturas digitais italianas, quer dizer daquelas comunidades que se colocam criticamente ao respeito dos mecanismos de desenvolvimento das tecnologías nas nossas sociedades. Mais não simplesmente isso, é muito mais. A gente sussurra no seu ouvido e só no seu, você não precisa contar para ninguém: o hackit é só para hackers de verdade, ou seja, para quem quer administrar sua vida como prefere e sabe comprometer-se a lutar por isso. Mesmo que nunca tenha visto um computador na sua vida.

Três dias de seminários, jogos, debates, trocas de ideias e aprendizagem coletivo, para analisarmos juntos as tecnologías que usamos todos os dias, como elas mudam e quais profundas mudanças induzem em nossas vidas reais e virtuais, que papel podemos desempenhar no direcionamento dessas mudanças para libertá-las do controle daqueles que querem monopolizar seu desenvolvimento, desintegrando o tecido social e nos relegando a espaços virtuais cada vez mais estreitos. 
O evento é totalmente autogerido: não há organizador@s e usuári@s, só participantes.
